
### 用户验证
	curl -X POST -vu clientapp:123456 http://localhost:8089/oauth/token -H "Accept: application/json" -d  "password=123456&username=mytest&grant_type=password&scope=read%20write&client_secret=123456&client_id=clientapp

### 更新令牌
	curl -X POST -vu clientapp:123456 http://localhost:8089/oauth/token -H "Accept: application/json" -d  "grant_type=refresh_token&refresh_token=134cb18f-101f-4bac-80eb-946211e39170&client_secret=123456&client_id=clientapp

### 注册用户
	curl -X POST  -d "username=xiaohua&password=123" localhost:8089/user

