package com.hudak.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "system")
public class SystemConfig {
	
	private Boolean smsEnable;

	public Boolean getSmsEnable() {
		return smsEnable;
	}

	public void setSmsEnable(Boolean smsEnable) {
		this.smsEnable = smsEnable;
	}
	
}
