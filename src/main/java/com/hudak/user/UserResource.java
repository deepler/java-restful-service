package com.hudak.user;

import org.springframework.hateoas.Resource;

public class UserResource extends Resource<User> {
	UserResource(User user) {
		super(user);
	}
}
