package com.hudak.user;

import org.jboss.logging.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.rich.RichGauge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hudak.util.Md5Utils;
import com.wordnik.swagger.annotations.Api;

import net.sf.json.JSONObject;


@Api(value = "user", description = "用户",position=3) // Swagger annotation
@RestController
@ExposesResourceFor(User.class)
@RequestMapping("/user")
public class UserAction {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserResourceAssembler userResourceAssembler;
	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public PagedResources<UserResource> listUsers(@AuthenticationPrincipal User user, Pageable pageable,
			PagedResourcesAssembler<User> assembler) {
		Page<User> users = userRepository.findAll(pageable);
		return assembler.toResource(users, userResourceAssembler);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{uid}",produces = MediaType.APPLICATION_JSON_VALUE)
	public UserResource getUser(@PathVariable Long uid) {
		User user = userRepository.findById(uid);
		UserResource userResource = userResourceAssembler.toResource(user);
		return userResource;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody Register register) {
		JSONObject  json = new JSONObject();
		User user = userRepository.findByUsername(register.getUsername());
		if (user != null) {
			json.put("state", "registered");
			return new ResponseEntity<JSONObject>(json, HttpStatus.UNPROCESSABLE_ENTITY);
		}
		
		
		user = new User();
		
		Md5PasswordEncoder md5PasswordEncoder =  new Md5PasswordEncoder();
		int salt = Md5Utils.getRandomSalt();
		String rawPass = md5PasswordEncoder.encodePassword(register.getPassword(), salt);
		user.setPassword(rawPass);
		user.setSalt(salt+"");
		user.setUsername(register.getUsername());
		user.setCreateTime(System.currentTimeMillis());
		user = userRepository.save(user);
		json.put("state", "success");
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}
	
}
